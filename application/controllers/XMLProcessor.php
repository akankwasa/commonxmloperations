<?php
/**
 * Created by IntelliJ IDEA.
 * User: Akankwasa Brian
 * Date: 12/19/2018
 * Time: 9:14 PM
 */

class XMLProcessor{

    /**
     * @param $xml
     * @return mixed|null
     * will return an associative array if the string passed is valid or return null when the string is invalid
     */
    public function ConvertXmlToArray($xml){
        libxml_use_internal_errors(true);

        $isValidXml=$this->CheckValidXml($xml);
        if(!$isValidXml){
            return null;
        }

        $simpleXmlString=simplexml_load_string($xml);
        $simpleXmlStringJsonEncoded=json_encode($simpleXmlString);
        $simpleXmlStringJsonDecoded=json_decode($simpleXmlStringJsonEncoded, 1);

        return $simpleXmlStringJsonDecoded;
    }

    /**
     * @param $xmlString
     * @return bool
     * tests if the xml string being passed is correct
     */
    public function CheckValidXml($xmlString){
        libxml_use_internal_errors(true);
        $xmlResult=@simplexml_load_string($xmlString);
        return $xmlResult?true:false;
    }


    /**
     * @param $data
     * @param $rootElementName
     * @param $result
     * @return string
     * assumes the array being passed  can only have one child who is an array and a child who children are arrays
     * else you would need to supply the array root tag for the children arrays
     * the result result param is passed by reference so no need of return statement
     */
    public function ConvertArrayToXml($data, $rootElementName, &$result){
        $result="";
        $result.="<".$rootElementName.">";
        foreach ($data as $key=>$value){
            $result.="<".$key.">";
            if(!is_array($value)){
                $result.=$value;
            }else{
                $result.=$this->arrayToXmlNodes($value);
            }
            $result.="</".$key.">";
        }

        $result.="</".$rootElementName.">";
    }

    /**
     * @param $arrayContainingInformation
     * @return string
     * assumess an asscoiative array is passed and it will converted into xml children or you can recursively the ConvertArrayToXml function
     */
    public function arrayToXmlNodes($arrayContainingInformation){
        $res="";
        foreach ($arrayContainingInformation as $key=>$value){
            $res.="<".$key.">";
                $res.=$value;
            $res.="</".$key.">";
        }
        return $res;
    }


}


